# A lxd based development environment 

Taking notes from this url

https://discuss.linuxcontainers.org/t/using-lxd-to-create-development-environments-for-virtualization/16778

## Steps

- Setup

  ```
  bash -x virt
  bash -x profile
  bash -x cloud-user
  lxc profile set "$USER" user.user-data "$(cat "$USER"-cloud-config.yml)
  lxc launch ubuntu:20.04 "${USER}-dev" -p default -p virt -p "$USER"
  ```

- Start

  ```
  lxc exec "${USER}-dev" su - "${USER}"
  ```
